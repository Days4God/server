# TODO
Setup:
* WebPack
* Babel
* TypeScript

# Server

## Goal
Act as a single source of truth for Days4God applications. The Web, and Android/IOS applications.

### Hosting Options

* Azure
* AWS
* Digital Ocean
* Kubernetes

### Technologies
* Docker
* TypeScript
* Redis
* NodeJS
* TypeORMk
* GraphQL
* PRISMA

#### Libraries
* Bcrypt
* CSRF
* XSS

### Starting Server
<!-- * Run `tsc -b -w` and `npm start` -->