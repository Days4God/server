import express from 'express';

import { WelcomeController } from './controllers';

const app: any = express();
const port: any = process.env.PORT || 8000;

app.use('/', WelcomeController);

app.use('/welcome', WelcomeController);

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`);
});